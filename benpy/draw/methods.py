import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.collections import LineCollection


def _dfs( graph, start, end ):
    #stackoverflow: https://stackoverflow.com/questions/40833612/find-all-cycles-in-a-graph-implementation
    """Depth first search algorithm, used to find cycles in the adjacency list"""
    fringe = [(start, [])]
    while fringe:
        state, path = fringe.pop()
        if path and state == end:
            yield path
            continue
        for next_state in graph[state]:
            if next_state in path:
                continue
            fringe.append((next_state, path + [next_state]))


def _get_segments(polygon):
        """"""
        ext_dir = [i for i, vt in enumerate(polygon.vertex_type) if vt == 0]
        g = {}
        for i, adj_list in enumerate(polygon.adj):
            if i not in ext_dir:
                tmp = [x for x in adj_list if x not in ext_dir]
                g[i] = tmp
        cycles = set([tuple(sorted(path)) for node in g for path in _dfs(g, node, node)])
        c_list = sorted(list(cycles),key=len,reverse=True)
        ref = []
        for x in c_list:
            flag = True
            for y in ref:
                if set(x).issubset(set(y)):
                    flag = False
            if flag:
                ref.append(x)
        return ref


def draw3d(polygon, norm_facts=None, facecolor="yellow", alpha=0.4, edgecolor="black"):
    """ Returns a 3D figure of the Polygon"""
    segments = _get_segments(polygon)
    if norm_facts is None:
        norm_facts = [1, 1, 1]
    points = [np.divide(p, norm_facts) for p in polygon.vertex_value]

    min_points = [ np.inf, np.inf, np.inf]
    max_points = [-np.inf,-np.inf,-np.inf]

    for i,p in enumerate(points):
        if polygon.vertex_type[i] == 1:
            min_points[0] = min(min_points[0],p[0])
            min_points[1] = min(min_points[1],p[1])
            min_points[2] = min(min_points[2],p[2])
            max_points[0] = max(max_points[0],p[0])
            max_points[1] = max(max_points[1],p[1])
            max_points[2] = max(max_points[2],p[2])

    list_p3d = [[points[x] for x in s] for s in segments]
    pd = Poly3DCollection(list_p3d)
    fig = plt.figure(figsize=(9, 10))
    ax = fig.add_subplot(111, projection='3d')
    pd.set_facecolor(facecolor)
    pd.set_alpha(alpha)
    pd.set_edgecolor(edgecolor)
    ax.add_collection3d(pd)
    print(min_points)
    print(max_points)
    ax.set_xlim(left=min_points[0], right=max_points[0])
    ax.set_ylim(bottom=min_points[1], top=max_points[1])
    ax.set_zlim(bottom=min_points[2], top=max_points[2])

    return fig, ax


def draw2d(polygon, norm_facts=None,color='black',linestyle='solid'):
    """ Returns a 2D figure of the Polygon"""
    segments = _get_segments(polygon)
    if norm_facts is None:
        norm_facts = [1, 1]
    points = [np.divide(p, norm_facts) for p in polygon.vertex_value]
    list_p2d = [[points[x] for x in s] for s in segments]
    lines = LineCollection(list_p2d, color=color, linestyle=linestyle)
    fig, ax = plt.subplots(figsize=(9, 10))
    ax.add_collection(lines)
    min_points = [ np.inf, np.inf]
    max_points = [-np.inf,-np.inf]

    for i,p in enumerate(points):
        if polygon.vertex_type[i] == 1:
            min_points[0] = min(min_points[0], p[0])
            min_points[1] = min(min_points[1], p[1])
            max_points[0] = max(max_points[0], p[0])
            max_points[1] = max(max_points[1], p[1])

    ax.set_xlim(left=min_points[0], right=max_points[0])
    ax.set_ylim(bottom=min_points[1], top=max_points[1])

    return fig, ax

def drawNd(polygon):
    """"""
