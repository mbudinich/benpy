import os
import numpy
from setuptools import setup, find_packages, Extension
solve_ext = ".pyx"
try:
    from Cython.Build import cythonize
except ImportError:
    solve_ext = ".c"

solve_src = ["benpy/solver/solver" + solve_ext] + ["benpy/solver/bensolve-mod/bslv_main.c",
                                                   "benpy/solver/bensolve-mod/bslv_vlp.c",
                                                   "benpy/solver/bensolve-mod/bslv_algs.c",
                                                   "benpy/solver/bensolve-mod/bslv_lists.c",
                                                   "benpy/solver/bensolve-mod/bslv_poly.c",
                                                   "benpy/solver/bensolve-mod/bslv_lp.c"]

ext = Extension("benpy.solver",
                sources=solve_src,
                include_dirs=[numpy.get_include(), os.environ['CONDA_PREFIX'] + "/include"],
                libraries=['glpk', 'm'],
                extra_compile_args=['-std=c99', '-O3']
                )
setup(
    name="benpy",
    version='1.1',
    description='Python Benpy Utility',
    author='Marko Budinich',
    author_email='marko.budinich@ls2n.fr',
    url='https://gitlab.univ-nantes.fr/mbudinich/benpy',
    license='GPLv3',
    long_description='Benpy',
    packages=find_packages(),
    ext_modules=cythonize([ext]),
    platforms='linux, OSX', install_requires=['numpy', 'prettytable', 'scipy']
)
